<?php

/*
 *Le decorator est un design pattern decorator
 */

namespace App;

/**
 * Description of CavaDecorator
 *
 * @author koby
 */
class CavaDecorator {
    //put your code here
    protected $hello;
    public function __construct(Hello $hello) {
        $this->hello=$hello;       
    }
    
    
    public function sayHello(){
        return $this->hello->sayHello().' .Comment ca va?';
    }
}
