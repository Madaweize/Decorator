<?php

require 'vendor/autoload.php';

$hello=new App\Hello();

$sayHello=new App\CavaDecorator($hello);

echo $sayHello->sayHello();